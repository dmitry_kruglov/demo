#include <stdio.h>

#define SWAP(a, b) (((a) ^= (b)), ((b) ^= (a)), ((a) ^= (b)))

int main(int argc, char* argv[]){
	int a = 13;
	int b = 25;

	printf("Before %d, %d!\n", a, b);	

	SWAP(a,b);

	printf("After %d, %d!\n", a, b);	

	return 0;
}
